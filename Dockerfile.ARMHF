# SYSROOT
FROM registry.gitlab.com/myriadrf/lime-suite:ARMHF-GLIBC-master-fan as sysroot

#
# sysroot with main build dependencies get produced here
#

RUN wget http://download.opensuse.org/repositories/network:/osmocom:/latest/Debian_9.0/Release.key \
	&& apt-key add Release.key  \
	&& echo "deb http://download.opensuse.org/repositories/network:/osmocom:/latest/Debian_9.0/ ./" | sudo tee /etc/apt/sources.list.d/osmocom-latest.list \
	&& apt-get update \
	&& apt-get install -y \
		libfftw3-dev libosmocore-dev libboost-all-dev \
		libusb-1.0-0-dev


# CROSSBUILDER
FROM registry.gitlab.com/pantacor/platform-tools/docker-glibc-cross-arm:master as crossbuilder
#
# cross toolchain docker for fast building of bits on x86
#

# we use the sysroot from above
COPY --from=sysroot / /sysroot-arm

# copy over just what we need
COPY osmo-trx osmo-trx
COPY .git .git
COPY .gitmodules .gitmodules

# install a few hosttools so we can do autoreconf
RUN apt-get update && apt-get install -y autoconf automake libtool; cd osmo-trx; \
	autoreconf -i -f

# build osmo-trx cross; bandaid because of bad --with-sysroot flag
RUN cd osmo-trx; mkdir build/; cd build/; \
	CC=arm-linux-gnueabihf-gcc \
	CXX=arm-linux-gnueabihf-g++ \
	LD=arm-linux-gnueabihf-ld \
	PKG_CONFIG_SYSROOT_DIR=/sysroot-arm \
	CPPFLAGS=-I/sysroot-arm/usr/include \
	CFLAGS="-O2 -mcpu=cortex-a7  -mfpu=neon-vfpv4" \
	CXXFLAGS="-O2 -mcpu=cortex-a7  -mfpu=neon-vfpv4" \
	LDFLAGS="--sysroot=/sysroot-arm -Wl,-rpath-link /sysroot-arm/usr/lib/arm-linux-gnueabihf -Wl,-rpath-link /sysroot-arm/lib/arm-linux-gnueabihf"\
	PKG_CONFIG_PATH=/sysroot-arm/usr/lib/arm-linux-gnueabihf/pkgconfig:/sysroot-arm/usr/local/lib/pkgconfig/ \
	../configure --with-lms --without-uhd \
		--host=arm-linux-gnueabihf --with-sysroot=/sysroot-arm && \
	make V=1 && make install DESTDIR=/stage


FROM registry.gitlab.com/pantacor/pantavisor-runtime/pvlogger:ARM32V6-master as pvlogger

## FINAL DOCKER ARTIFACT IS HERE
FROM registry.gitlab.com/myriadrf/lime-suite:ARMHF-GLIBC-master-fan

RUN wget http://download.opensuse.org/repositories/network:/osmocom:/latest/Debian_9.0/Release.key \
	&& apt-key add Release.key  \
	&& echo "deb http://download.opensuse.org/repositories/network:/osmocom:/latest/Debian_9.0/ ./" | sudo tee /etc/apt/sources.list.d/osmocom-latest.list \
	&& apt-get update; apt-get install -y osmo-bts-trx osmocom-nitb libosmogsm11 \
	&& apt-get install -y libfftw3-3 libusb-1.0-0 \
		openssh-server \
		raspi-config \
		rsyslog \
		logrotate \
		vim-tiny \
	&& apt-get clean && rm -rf /var/cache/apt/lists \
	&& mkdir -p /root/.ssh \
	&& echo root:pantalime | chpasswd \
	&& sed -i 's/#ForwardToSyslog=yes/ForwardToSyslog=yes/' /etc/systemd/journald.conf \
	&& sed -i 's/#Port 22/Port 7024/;s/.*PermitRootLogin.*$/PermitRootLogin yes/;/.*PasswordAuthentication.*/d;$aPasswordAuthentication no\n' /etc/ssh/sshd_config

COPY files/ /
COPY --from=pvlogger /usr/local/bin/pvlogger /usr/local/bin/

RUN mkdir -p /etc/systemd/system/multi-user.target.wants/ || true \
	&& ln -s /etc/systemd/regenerate_ssh_host_keys.service /etc/systemd/system/multi-user.target.wants/ \
	&& ln -s /etc/systemd/system/pvlogger-syslog.service /etc/systemd/system/multi-user.target.wants/ \
	&& ln -s /etc/systemd/system/lime-gsmstation-configs.path /etc/systemd/system/multi-user.target.wants/ \
	&& ln -s /lib/systemd/system/logrotate.timer /etc/systemd/system/timers.target.wants/logrotate.timer \
	&& rm -f /etc/systemd/system/timers.target.wants/apt-daily-upgrade.timer \
	&& rm -f /etc/systemd/system/timers.target.wants/apt-daily.timer

COPY --from=crossbuilder /stage/usr/local /usr/local
COPY --from=pvlogger /usr/local/bin/pvlogger /usr/local/bin/

CMD [ "/bin/systemd" ]
